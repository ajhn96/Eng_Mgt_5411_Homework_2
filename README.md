### Homework 2: EM 5411 ###

#### Problem 1. ####
Do three full iterations by hand, that is, provide detailed solutions. Show all computations and steps. Use the Newton’s method to solve the problems with $`ε=0.01`$, start at the midpoint of the range provided. If the stopping criteria are met prior to the third iterations state so. Thus, you should have solved x0, x1, and x2. 

a. $`minimize{f(x)=105x+8/x-45:0<=x<=1}`$

b. $`maximize{f(x)=4x^3-7x^2+14x+6:0<=x<=1}`$

#### Problem 2: ####
Use the Newton’s method to solve the problems with $`ε=0.01`$, start at the midpoint of the range provided. Use code or provide your own code to complete and fully solve these problems.

a. $`minimize{f(x)=105x+8/x-45:0<=x<=1}`$

b. $`minimize{f(x)=3x^2-5x-4:0<=x<=2}`$

c. $`minimize{f(x)=-2x^4-2x-1:)<=x<=1}`$

d. $`maximize{f(x)=4x^3-7x^2+14x+6:0<=x<=1}`$


#### Problem 3: ####
Use Newton’s Method to $`minimize{f(x)=x_1^2-4x_1+2x_2^22x^2+(x_1x_2)^(3/2)}`$
Similar to problem 1 do three full detailed iterations by hand.